﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace RegistroDePersonas.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class RegistroDePersonasController : ControllerBase
    {
        // GET: api/RegistroDePersonas
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[]
            {
                "value1",
                "value2" 
            };
        }

        // GET: api/RegistroDePersonas/5
        [HttpGet("{id}", Name = "Get")]
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/RegistroDePersonas
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT: api/RegistroDePersonas/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
